from random import choice,randint
import os
import time

class Personajes:

	def __init__(self,Nombre,PuntosVida,PuntosAtaque):
		self.Nombre=Nombre
		self.PuntosVida = PuntosVida
		self.PuntosAtaque = PuntosAtaque


	def __str__(self):
		return "{} vida y {} poder de ataque".format(self.PuntosVida,self.PuntosAtaque)

class Enemigo(Personajes):

	def __init__(self,Nombre,PuntosVida,PuntosAtaque):
		super().__init__(Nombre,PuntosVida,PuntosAtaque)

	def __str__(self):
		return 'Enemigo: {} '.format(self.Nombre) + super().__str__()

	def reestablecerVidaEnemigos(self,VidaMaxima):
		self.PuntosVida=VidaMaxima

class Dragon(Enemigo):

	def __init__(self,Nombre,PuntosVida,PuntosAtaque):
		super().__init__(Nombre,PuntosVida,PuntosAtaque)
		self.VidaMaxima = 150
	
	def HabilidadEspecialDragonNormal(self):
		""" Aliento del Dragon """
		print("El {} uso su fuego contra {}".format(EnemigoDragon.Nombre,Unreal.Nombre))
		Unreal.PuntosVida-=30 

	def DropeoOro(self):
		OroGanado = randint(20,30)
		return OroGanado

	def DropeoExp(self):
		ExpGanada = randint(10,20)
		return ExpGanada

class DragonFuego(Dragon):
	def __init__(self,Nombre,PuntosVida,PuntosAtaque):
		super().__init__(Nombre,PuntosVida,PuntosAtaque)
		self.VidaMaxima = 175

	def HabilidadEspecialDragonFuego(self,Enemigo):
		""" Llamarada del Dragon"""
		print("{} lanzo Llamarada del Dragon infligiendo 40 puntos de daño a {}".format(Enemigo.Nombre,Unreal.Nombre))
		Unreal.PuntosVida-=40

	def DropeoOro(self):
		OroGanado = randint(20,30)
		return OroGanado

	def DropeoExp(self):
		ExpGanada = randint(10,20)
		return ExpGanada

	def DropeoMedallon(self):
		print("Has obtenido el Medallon de Fuego!")
		inventario.MedallonFuego = True


class DragonHielo(Dragon):
	def __init__(self,Nombre,PuntosVida,PuntosAtaque):
		super().__init__(Nombre,PuntosVida,PuntosAtaque)
		self.VidaMaxima = 175

	def HabilidadEspecialDragonHielo(self,Enemigo):
		""" Aliento del Dragon"""
		print("{} lanzo Aliento del Dragon curandose 40 puntos de vida".format(Enemigo.Nombre))
		Enemigo.PuntosVida+=40

	def DropeoOro(self):
		OroGanado = randint(20,30)
		return OroGanado

	def DropeoExp(self):
		ExpGanada = randint(10,20)
		return ExpGanada

	def DropeoMedallon(self):
		print("Has obtenido el Medallon de Hielo!")
		inventario.MedallonHielo = True

class DragonTierra(Dragon):
	def __init__(self,Nombre,PuntosVida,PuntosAtaque):
		super().__init__(Nombre,PuntosVida,PuntosAtaque)
		self.VidaMaxima = 175

	def HabilidadEspecialDragonTierra(self,PuntosAtaqueJugador,Enemigo):
		""" Escudo de Tierra (Lo mismo que el esquivar pero con escudo)"""
		print("{} lanzo Escudo de Tierra absorviendo todo el ataque de {}".format(Enemigo.Nombre,Unreal.Nombre))
		return PuntosAtaqueJugador

	def DropeoOro(self):
		OroGanado = randint(20,30)
		return OroGanado

	def DropeoExp(self):
		ExpGanada = randint(10,20)
		return ExpGanada

	def DropeoMedallon(self):
		print("Has obtenido el Medallon de Tierra!")
		inventario.MedallonTierra = True

class DragonAire(Dragon):
	def __init__(self,Nombre,PuntosVida,PuntosAtaque):
		super().__init__(Nombre,PuntosVida,PuntosAtaque)
		self.VidaMaxima = 175

	def HabilidadEspecialDragonAire(self,Enemigo):
		"""Ataques extra (Entre 1 y 2(60%->1; 40%->2  (Poner que haga 20 daño))) """
		print("{} uso el poder del Aire para adelantarse a los ataques de {}".format(Enemigo.Nombre,Unreal.Nombre))
		ProbabilidadNumerosDeAtaque = 0
		ProbabilidadNumerosDeAtaque = randint(0,100)

		if ProbabilidadNumerosDeAtaque <= 60:
			print("{} realiza un ataque rapido de 20 puntos de daño".format(self.Nombre))
			Unreal.PuntosVida -= 20
		else:
			print("{} realiza un doble ataque rapido de 40 puntos de daño".format(self.Nombre))
			Unreal.PuntosVida -= 40

		if self.PuntosVida <= 0:
			print("Has sido derrotado por el {}".format(Enemigo.Nombre))
			print(" ")
			print("Fin del Juego.")
			print(" ")
			os.system('pause')
			print(" ")
			exit()	

	def DropeoOro(self):
		OroGanado = randint(20,30)
		return OroGanado

	def DropeoExp(self):
		ExpGanada = randint(10,20)
		return ExpGanada

	def DropeoMedallon(self):
		print("Has obtenido el Medallon de Aire!")
		inventario.MedallonAire = True

class DragonElemental(DragonFuego,DragonHielo,DragonAire,DragonTierra):
	""" Hereda 5 ataques especiales,el del Dragon Normal
	    y el de los 4 otros Dragones """

	def __init__(self,Nombre,PuntosVida,PuntosAtaque):
		super().__init__(Nombre,PuntosVida,PuntosAtaque)	
		self.VidaMaxima = 200

	pass

class Goblin(Enemigo):

	def __init__(self,Nombre,PuntosVida,PuntosAtaque):
		super().__init__(Nombre,PuntosVida,PuntosAtaque)
		self.VidaMaxima = 75

	def HabilidadEspecial(self,PuntosAtaque):  #Ataque rapido
		Unreal.PuntosVida -= PuntosAtaque
		print("{} realiza un ataque rapido de {} puntos de daño".format(self.Nombre,self.PuntosAtaque))
		if self.PuntosVida <= 0:
			print("Has sido derrotado por el {}".format(Enemigo.Nombre))
			print(" ")
			print("Fin del Juego.")
			print(" ")
			os.system('pause')
			print(" ")
			exit()			

	def DropeoOro(self):
		OroGanado = randint(10,20)
		return OroGanado

	def DropeoExp(self):
		ExpGanada = randint(2,4)
		return ExpGanada

class Elfo(Enemigo):

	def __init__(self,Nombre,PuntosVida,PuntosAtaque):
		super().__init__(Nombre,PuntosVida,PuntosAtaque)
		self.VidaMaxima = 50

	def HabilidadEspecial(self,PuntosAtaqueJugador):  #Esquivar Golpe
		print("{} ha esquivado el ataque".format(self.Nombre))
		return PuntosAtaqueJugador
	
	def DropeoOro(self):
		OroGanado = randint(5,10)
		return OroGanado

	def DropeoExp(self):
		ExpGanada = randint(3,6)
		return ExpGanada

class Orco(Enemigo):

	def __init__(self,Nombre,PuntosVida,PuntosAtaque):
		super().__init__(Nombre,PuntosVida,PuntosAtaque)
		self.VidaMaxima = 100

	def HabilidadEspecial(self):  #Golpe Critico     """ Crea subclase lllamada OrcoBerserker que aumente el daño cuando le quede poca vida """
		print("{} ha realizado un golpen critico".format(EnemigoOrco.Nombre))
		return EnemigoOrco.PuntosAtaque
	
	def DropeoOro(self):
		OroGanado = randint(0,5)
		return OroGanado

	def DropeoExp(self):
		ExpGanada = randint(5,10)
		return ExpGanada

class Jugador(Personajes):
	
	def __init__(self,Nombre,PuntosVida,PuntosAtaque):
		super().__init__(Nombre,PuntosVida,PuntosAtaque)
		self.Pelea = False
		self.Exp = 0
		self.VidaMaxima = PuntosVida
		self.Nivel = 0
		self.ExpNecesariaSubirNivel = 10 #Falta poner que dependa de la dificultad
		self.Oro = 0
		self.EnemigosAsesinados = 0

	def estado(self):
		print("{} es Nivel {}".format(self.Nombre, self.Nivel))
		print("{} tiene {} puntos de vida".format(self.Nombre, self.PuntosVida))
		print("{} posee {} puntos de experiencia".format(self.Nombre,self.Exp))
		print("{} posee {} monedas de oro".format(self.Nombre, self.Oro))
		print("Has asesinado a {} enemigos".format(self.EnemigosAsesinados))

	def ganar_oro(self, oro_recibido):
		self.Oro+=oro_recibido

	def experiencia(self,PuntosExp):
		self.Exp+=PuntosExp

	def niveles(self):
		if self.Exp >= self.ExpNecesariaSubirNivel:
			print("{} ha subido de nivel".format(self.Nombre))
			self.Nivel+=1
			self.ExpNecesariaSubirNivel+=10 #Falta poner que dependa de la dificultad
			self.VidaMaxima+=10

	def tienda(self):
		print("Bienvenido a la Tienda")
		print("¿Que desa comprar? (Solo puedes realizar una compra)")
		print("1 -> Aumento de 5 Puntos de Vida (15 oro)")
		print("2 -> Aumento de 5 Puntos de Ataque (30 oro)")
		print("Tu Oro actual: {}".format(self.Oro))
		print("Pulse cualquier otra tecla para salir de la tienda")
		comprar = input("")

		if comprar == "1":
			if self.Oro >= 15:
				self.Oro -= 15
				self.VidaMaxima += 5
				self.PuntosVida += 5
				print("Gracias por su compra :D")
			else:
				print("No tienes suficiente oro")	
		elif comprar == "2":
			if self.Oro >= 30:
				self.Oro -= 30
				self.PuntosAtaque += 5
				print("Gracias por su compra :D")
			else:
				print("No tienes suficiente oro")
		else:
			print("Gracias por su estancia en la tienda")
			print("Vuelve cuando quieras")

	def explorar(self):
		print("Explorando la zona")

		for i in range(5):
			time.sleep(1)
			print(".")

		exploracion = randint(0,100)

		if exploracion>=0 and exploracion<=15:   #15 Cofre con Oro
			self.Pelea = False
			GanarOro = randint(5,20)
			Unreal.ganar_oro(GanarOro)
			print("Has encontrado un cofre con {} monedas de oro".format(GanarOro))
		elif exploracion>15 and exploracion<=30: #15 Tienda
			self.Pelea = False
			print("Has encontrado una tienda")

			for i in range(3):
				time.sleep(1)
				print("Entrando a la tienda...")

			Unreal.tienda()
		elif exploracion>30 and exploracion<=60: #30 No encontrar nada
			print("No has encontrado nada")
			self.Pelea = False
		else:									 #40 Enfrentamiento con un Enemigo
			self.Pelea = True

	def descansar(self):
		print("Descansando de un dia agotador")

		for i in range(5):
			time.sleep(1)
			print(".")

		self.PuntosVida = self.VidaMaxima

		print("Has recuperado puntos de vida")
		print("Listo para continuar")
		print(" ")

	def __str__(self):
		return 'Jugador: {} '.format(self.Nombre) + super().__str__()

	def HabilidadEspecial(self):
		self.PuntosVida+=self.PuntosAtaque
		print("{} se ha curado {} puntos de vida".format(self.Nombre,self.PuntosAtaque))

	def enfrentamiento(self,Enemigo):
		time.sleep(2)
		print("Te has encontrado con un Enemigo")
		print(" ")
		while True:

			time.sleep(2)

			print("-------------------------------------------------------")
			print("{} con {} puntos de vida y {} poder de ataque".format(self.Nombre,self.PuntosVida,self.PuntosAtaque))
			print("{} con {} puntos de vida y {} poder de ataque".format(Enemigo.Nombre,Enemigo.PuntosVida,Enemigo.PuntosAtaque))
			print("-------------------------------------------------------")

			time.sleep(2)

			#--------Variables-------------------
			ReduccionDaño = 0
			GolpeCritico = 0
			ProbabilidadEnemigo = 0
			ProbabilidadHabilidadEspecialBossFinal = 0
			#------------------------------------

			#-----------------Habilidades Especiales Enemigos----------------------
			if Enemigo == EnemigoElfo:
				ProbabilidadEnemigo = randint(0,100)
				if ProbabilidadEnemigo <= 20:
					ReduccionDaño = Enemigo.HabilidadEspecial(self.PuntosAtaque)
			if Enemigo == EnemigoGoblin:
				ProbabilidadEnemigo = randint(0,100)
				if ProbabilidadEnemigo <= 20:
					Enemigo.HabilidadEspecial(Enemigo.PuntosAtaque)
			if Enemigo == EnemigoDragonTierra:
				ProbabilidadEnemigo = randint(0,100)
				if ProbabilidadEnemigo <= 20:
					ReduccionDaño = EnemigoDragonTierra.HabilidadEspecialDragonTierra(self.PuntosAtaque,EnemigoDragonTierra)
			if Enemigo == EnemigoDragonAire:
				ProbabilidadEnemigo = randint(0,100)
				if ProbabilidadEnemigo <= 20:
					EnemigoDragonAire.HabilidadEspecialDragonAire(EnemigoDragonAire)
			#------------------------------------------------------------------------
			#-----------------Habilidades Especiales Boss Final----------------------
			if Enemigo == EnemigoDragonElemental:
				ProbabilidadEnemigo = 0
				ProbabilidadEnemigo = randint(0,100)
				if ProbabilidadEnemigo <= 20:
					ProbabilidadHabilidadEspecialBossFinal = 0
					ProbabilidadHabilidadEspecialBossFinal = randint(0,100)
					if ProbabilidadHabilidadEspecialBossFinal <= 50:
						ReduccionDaño = EnemigoDragonElemental.HabilidadEspecialDragonTierra(self.PuntosAtaque,EnemigoDragonElemental)
					else:
						EnemigoDragonElemental.HabilidadEspecialDragonAire(EnemigoDragonElemental)
			#----------------------------------------------------------------------

			Enemigo.PuntosVida -= (self.PuntosAtaque+GolpeCritico-ReduccionDaño)
			print("{} infligue {} de daño al {}".format(self.Nombre,(self.PuntosAtaque+GolpeCritico-ReduccionDaño), Enemigo.Nombre))

			#-------------------Muerte del Enemigo----------------------------------
			if Enemigo.PuntosVida <= 0:
				if Enemigo == EnemigoDragonElemental:
					print(" ")
					print("Has vencido al {}".format(Enemigo.Nombre))
					for i in range(10):
						time.sleep(1)
						print(":D")
					print("Enhorabuena, has derrotado al Boss Final de este juego.")
					os.system("Pause")
					print("")
					Unreal.estado()
					print("")
					time.sleep(3)
					print("Espero que hallas disfrutado este juego tanto como yo")
					print("lo he disfrutado programando")
					time.sleep(2)
					print("PD: Seguire metiendole contenido :P")
					os.system('pause')
					print("Pulsa cualquier tecla para finalizar el juego")
					os.system('pause')
					exit()
				else:
					print(" ")
					print("Has vencido al {}".format(Enemigo.Nombre))
					self.EnemigosAsesinados+=1
					Enemigo.reestablecerVidaEnemigos(Enemigo.VidaMaxima)
					OroGanado = Enemigo.DropeoOro()
					Unreal.ganar_oro(OroGanado)
					print("Has ganado {} monedas de oro".format(OroGanado))
					PuntitosExp = Enemigo.DropeoExp()
					Unreal.experiencia(PuntitosExp)
					print("Has ganado {} puntos de experiencia".format(PuntitosExp))
					Unreal.niveles()
					if Enemigo == EnemigoDragonFuego and MedallonFuego == False:
						EnemigoDragonFuego.DropeoMedallon()
					elif Enemigo == EnemigoDragonHielo and MedallonHielo == False:
						EnemigoDragonHielo.DropeoMedallon()
					elif Enemigo == EnemigoDragonTierra and MedallonTierra == False:
						EnemigoDragonTierra.DropeoMedallon()
					elif Enemigo == EnemigoDragonAire and MedallonAire == False:
						EnemigoDragonAire.DropeoMedallon()

				print(" ")
				break
			#--------------------------------------------------------------------

			#-----------------Habilidades Especiales Jugador----------------------
			ProbabilidadJugador = randint(0,100)
			if ProbabilidadJugador <= 20:
				self.HabilidadEspecial()
			#-----------------------------------------------------------------------
			ProbabilidadEnemigo = 0
			#-----------------Habilidades Especiales Enemigos----------------------
			if Enemigo == EnemigoOrco:
				ProbabilidadEnemigo = randint(0,100)
				if ProbabilidadEnemigo <= 20:
					GolpeCritico = Orco.HabilidadEspecial(self)
			if Enemigo == EnemigoDragon:
				ProbabilidadEnemigo = randint(0,100)
				if ProbabilidadEnemigo <= 20:
					EnemigoDragon.HabilidadEspecialDragonNormal()
			if Enemigo == EnemigoDragonFuego:
				ProbabilidadEnemigo = randint(0,100)
				if ProbabilidadEnemigo <= 20:
					EnemigoDragonFuego.HabilidadEspecialDragonFuego(EnemigoDragonFuego)
			if Enemigo == EnemigoDragonHielo:
				ProbabilidadEnemigo = randint(0,100)
				if ProbabilidadEnemigo <= 20:
					EnemigoDragonHielo.HabilidadEspecialDragonHielo(EnemigoDragonHielo)
			#------------------------------------------------------------------------

			#-----------------Habilidades Especiales Boss Final----------------------
			if Enemigo == EnemigoDragonElemental:
				ProbabilidadEnemigo = 0
				ProbabilidadEnemigo = randint(0,100)
				if ProbabilidadEnemigo <= 20:
					ProbabilidadHabilidadEspecialBossFinal = 0
					ProbabilidadHabilidadEspecialBossFinal = randint(0,100)
					if ProbabilidadHabilidadEspecialBossFinal <= 50:
						EnemigoDragonElemental.HabilidadEspecialDragonFuego(EnemigoDragonElemental)
					else:
						EnemigoDragonElemental.HabilidadEspecialDragonHielo(EnemigoDragonElemental)
			#----------------------------------------------------------------------

			self.PuntosVida -= (Enemigo.PuntosAtaque+GolpeCritico)
			print("{} infligue {} de daño a {}".format(Enemigo.Nombre, (Enemigo.PuntosAtaque+GolpeCritico), self.Nombre))

			#-----------Muerte Jugador-----------------
			if self.PuntosVida <= 0:
				print("Has sido derrotado por el {}".format(Enemigo.Nombre))
				print(" ")
				print("Fin del Juego.")
				print(" ")
				os.system('pause')
				print(" ")
				exit()
				break
			#------------------------------------------

class Inventario():

	def __init__(self):
		self.MedallonFuego = False
		self.MedallonHielo = False
		self.MedallonAire = False
		self.MedallonTierra = False
		self.MedallonElemental = False

	def DropeoMedallonFuego(Dropeo):
		self.MedallonFuego = Dropeo

	def DropeoMedallonHielo(Dropeo):
		self.MedallonHielo = Dropeo

	def DropeoMedallonTierra(Dropeo):
		self.MedallonTierra = Dropeo

	def DropeoMedallonAire(Dropeo):
		self.MedallonAire = Dropeo


	def MostrarInventario(self,MedallonFuego,MedallonHielo,MedallonTierra,MedallonAire,MedallonElemental):
		if MedallonFuego == False and MedallonHielo == False and MedallonAire == False and MedallonTierra == False:
			print("Inventario vacio")
		if MedallonFuego == True and MedallonElemental == False:
			print("Posees el Medallon de Fuego")
		if MedallonHielo == True and MedallonElemental == False:
			print("Posees el Medallon de Hielo")
		if MedallonTierra == True and MedallonElemental == False:
			print("Posees el Medallon de Tierra")
		if MedallonAire == True and MedallonElemental == False:
			print("Posees el Medallon de Aire")
		if MedallonElemental == True:
			print("Posees el Medallon Elemental")

#--------------------------------------------------------------------------------------------------------------------
	
Nombre = input("¿Como se llama el/la heroe de esta aventura?")
time.sleep(3)
print(" ")

Unreal = Jugador(Nombre,100,20) #(100,20)
inventario = Inventario()

MedallonFuego = False
MedallonHielo = False
MedallonAire = False
MedallonTierra = False
MedallonElemental = False
OpcionFusionar = True

while True:
	print("""¿Que opcion elegira {}?
1 -> Explorar la zona
2 -> Descansar
3 -> Estado
4 -> Inventario""".format(Nombre))
	accion = input()

	if accion == "1":
		Unreal.explorar()
		if Unreal.Pelea:
			EnemigoOrco = Orco("Orco",100,20)
			EnemigoElfo = Elfo("Elfo",50,30)
			EnemigoGoblin = Goblin("Goblin",75,25)
			EnemigoDragon = Dragon("Dragon",150,30)
			EnemigoDragonFuego = DragonFuego("Dragon de Fuego",175,35)
			EnemigoDragonHielo = DragonHielo("Dragon de Hielo",175,35)
			EnemigoDragonAire = DragonAire("Dragon de Aire",175,35)
			EnemigoDragonTierra = DragonTierra("Dragon de Tierra",175,35)
			EnemigoDragonElemental = DragonElemental("Dragon Elemental",200,40)
			ListaEnemigos = [EnemigoOrco,EnemigoElfo,EnemigoGoblin]	
			ListaEnemigos2 = [EnemigoOrco,EnemigoElfo,EnemigoGoblin,EnemigoDragon]	
			if Unreal.EnemigosAsesinados < 10:	
				Unreal.enfrentamiento(choice(ListaEnemigos))
			elif Unreal.EnemigosAsesinados >= 10 and Unreal.EnemigosAsesinados < 15 and inventario.MedallonElemental == False:
				Unreal.enfrentamiento(choice(ListaEnemigos2))
			elif Unreal.EnemigosAsesinados >= 15 and inventario.MedallonElemental == False:
				ProbabilidadMiniBoss = randint(0,100)
				if ProbabilidadMiniBoss >= 0 and ProbabilidadMiniBoss < 5:
					Unreal.enfrentamiento(EnemigoDragonFuego)
				if ProbabilidadMiniBoss >= 5 and ProbabilidadMiniBoss < 10:
					Unreal.enfrentamiento(EnemigoDragonHielo)
				if ProbabilidadMiniBoss >= 10 and ProbabilidadMiniBoss < 15:
					Unreal.enfrentamiento(EnemigoDragonAire)
				if ProbabilidadMiniBoss >= 15 and ProbabilidadMiniBoss < 20:
					Unreal.enfrentamiento(EnemigoDragonTierra)
				if ProbabilidadMiniBoss >= 20:
					Unreal.enfrentamiento(choice(ListaEnemigos2))
			elif inventario.MedallonElemental == True:
				Unreal.enfrentamiento(EnemigoDragonElemental)

	if accion == "2":
		Unreal.descansar()
	if accion == "3":
		Unreal.estado()
	if accion == "4":
		if OpcionFusionar == True and inventario.MedallonFuego == True and inventario.MedallonTierra == True and inventario.MedallonAire == True and inventario.MedallonHielo == True:
			print("Has Obtenido los 4 Medallones ¿Quieres fusionarlos?")
			print("Pulsa 's' para Fusionar")
			Fusion = input("")
			if Fusion == "S" or Fusion == "s":
				inventario.MedallonElemental=True
				OpcionFusionar = False
		inventario.MostrarInventario(inventario.MedallonFuego,inventario.MedallonHielo,inventario.MedallonTierra,inventario.MedallonAire,inventario.MedallonElemental)
	print(" ")
	time.sleep(2)		